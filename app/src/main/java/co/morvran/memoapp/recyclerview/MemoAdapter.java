package co.morvran.memoapp.recyclerview;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import org.parceler.Parcels;

import java.util.Collections;
import java.util.List;

import co.morvran.memoapp.R;
import co.morvran.memoapp.activities.DetailsActivity;
import co.morvran.memoapp.fragment.DetailFragment;
import co.morvran.memoapp.metier.DataBaseHelper;
import co.morvran.memoapp.metier.dto.MemoDTO;

public class MemoAdapter extends RecyclerView.Adapter<MemoAdapter.MemoViewHolder> {

    private List<MemoDTO> allMemos;
    private AppCompatActivity mainActivity;

    public MemoAdapter(List<MemoDTO> allMemos, AppCompatActivity mainActivity)
    {
        this.allMemos = allMemos;
        this.mainActivity = mainActivity;
    }

    @Override
    public MemoViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View viewMemo = LayoutInflater.from(parent.getContext()).inflate(R.layout.memo_item_liste, parent, false);
        return new MemoViewHolder(viewMemo);
    }

    @Override
    public void onBindViewHolder(MemoViewHolder holder, int position)
    {
        holder.textViewContenuMemo.setText(allMemos.get(position).content);
    }

    @Override
    public int getItemCount()
    {
        return allMemos.size();
    }

    public boolean onItemMove(int positionDebut, int positionFin)
    {
        Collections.swap(allMemos, positionDebut, positionFin);
        notifyItemMoved(positionDebut, positionFin);
        return true;
    }

    public void onItemDismiss(int position)
    {
        if (position > -1)
        {
            DataBaseHelper.getDatabase(mainActivity).memoDAO().delete(allMemos.get(position));
            allMemos.remove(position);
            notifyItemRemoved(position);
        }
    }

    public class MemoViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewContenuMemo;

        public MemoViewHolder(View itemView)
        {
            super(itemView);
            textViewContenuMemo = itemView.findViewById(R.id.content_memo);

            textViewContenuMemo.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    MemoDTO memo = allMemos.get(getAdapterPosition());

                    //Mode paysage
                    if(mainActivity.findViewById(R.id.FragmentLayout) != null)
                    {
                        DetailFragment fragment = new DetailFragment();
                        //Passage de paramètres
                        Bundle bundle = new Bundle();
                        bundle.putString("content", memo.content);
                        fragment.setArguments(bundle);
                        //Ajout du fragment au layout de la main activity
                        FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.FragmentLayout, fragment, "detail_fragment");
                        fragmentTransaction.commit();
                    }
                    //Mode portrait
                    else
                    {
                        //Lancement d'une nouvelle activité et passage de l'objet mémo
                        Intent intent = new Intent(view.getContext(), DetailsActivity.class);
                        intent.putExtra("memo", Parcels.wrap(memo));
                        view.getContext().startActivity(intent);
                    }
                }
            });
        }

    }

}
