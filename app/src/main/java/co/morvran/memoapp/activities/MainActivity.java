package co.morvran.memoapp.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import co.morvran.memoapp.R;
import co.morvran.memoapp.callback.ItemTouchHelperCallback;
import co.morvran.memoapp.metier.DataBaseHelper;
import co.morvran.memoapp.metier.dto.MemoDTO;
import co.morvran.memoapp.recyclerview.MemoAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    List<MemoDTO> listMemoDTO;
    MemoAdapter memoAdapter;
    TextInputEditText inputMemo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DataBaseHelper.getDatabase(this);

        RecyclerView recyclerView = findViewById(R.id.RecyclerMemo);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        listMemoDTO = DataBaseHelper.getDatabase(this).memoDAO().getListMemo();
        memoAdapter = new MemoAdapter(listMemoDTO, this);
        recyclerView.setAdapter(memoAdapter);

        inputMemo = findViewById(R.id.InputMemo);

        Button btnOk = findViewById(R.id.BtnAdd);
        btnOk.setOnClickListener(this);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelperCallback(memoAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    @Override
    public void onClick(View view) {
        String textInput = inputMemo.getText().toString();
        if (view.getId() == R.id.BtnAdd && !textInput.matches("") ) {
            MemoDTO newMemo = new MemoDTO(inputMemo.getText().toString());
            DataBaseHelper.getDatabase(view.getContext()).memoDAO().insert(newMemo);
            listMemoDTO.add(newMemo);
            memoAdapter.notifyItemInserted(listMemoDTO.size());
            inputMemo.setText("");
        }else{
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("You need to complete the field!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }
}
