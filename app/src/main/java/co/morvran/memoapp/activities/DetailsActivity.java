package co.morvran.memoapp.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import org.parceler.Parcels;

import co.morvran.memoapp.R;
import co.morvran.memoapp.fragment.DetailFragment;
import co.morvran.memoapp.metier.dto.MemoDTO;

public class DetailsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        DetailFragment fragment = new DetailFragment();
        //Récupération de l'objet
        MemoDTO memo = Parcels.unwrap(getIntent().getParcelableExtra("memo"));
        //Passage au fragment
        Bundle bundle = new Bundle();
        bundle.putString("content", memo.content);
        fragment.setArguments(bundle);
        //Ajout du fragment au layout
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_fragment, fragment, "detail_fragment");
        fragmentTransaction.commit();
    }
}
