package co.morvran.memoapp.metier.dto;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.parceler.Parcel;

@Parcel
@Entity(tableName = "memo")
public class MemoDTO
{
    @PrimaryKey(autoGenerate = true)
    public long memoId = 0;
    public String content;

    public MemoDTO() {}

    public MemoDTO(String content)
    {
        this.content = content;
    }
}
