package co.morvran.memoapp.metier;

import android.content.Context;

import androidx.room.Room;

public class DataBaseHelper
{
    private static DataBaseHelper databaseHelper = null;
    private DataBase database;

    private DataBaseHelper(Context context)
    {
        database = Room
                .databaseBuilder(context, DataBase.class, "memo.db")
                .allowMainThreadQueries()
                .build();
    }

    public static synchronized DataBase getDatabase(Context context)
    {
        if(databaseHelper == null)
        {
            databaseHelper = new DataBaseHelper(context.getApplicationContext());
        }
        return databaseHelper.database;
    }
}
