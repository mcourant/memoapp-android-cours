package co.morvran.memoapp.metier;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import co.morvran.memoapp.metier.dao.MemoDAO;
import co.morvran.memoapp.metier.dto.MemoDTO;

@Database(entities = {MemoDTO.class}, version = 1)
public abstract class DataBase extends RoomDatabase
{
    public abstract MemoDAO memoDAO();
}

