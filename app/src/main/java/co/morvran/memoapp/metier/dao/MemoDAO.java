package co.morvran.memoapp.metier.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import co.morvran.memoapp.metier.dto.MemoDTO;

@Dao
public abstract class MemoDAO {

    @Query("SELECT * FROM memo")
    public abstract List<MemoDTO> getListMemo();

    @Insert
    public abstract void insert(MemoDTO... memo);

    @Delete
    public abstract void delete(MemoDTO... memo);
}
